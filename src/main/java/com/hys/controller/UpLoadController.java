package com.hys.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;

@Controller
public class UpLoadController {

    @RequestMapping("/uploadFile")
    @ResponseBody
    public String upload(@RequestParam("file") CommonsMultipartFile file, HttpSession session) throws IOException {

        System.out.println("name："+file.getName());
        System.out.println("文件大小："+file.getSize());
        System.out.println("文件类型："+file.getContentType());
        System.out.println("文件名："+file.getOriginalFilename());

        String realPath = session.getServletContext().getRealPath("/upload");
        File file1 = new File(realPath);
        if (!file1.exists()){
            file1.mkdirs();
        }

        String fileName = file.getOriginalFilename();
        String upload = realPath+"/"+fileName;

        file.transferTo(new File(upload));

        return "ok";
    }

}
