package com.hys.controller;

import com.hys.pojo.Dog;
import com.hys.pojo.Student;
import com.hys.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class CommController {
    @RequestMapping("/getHeader")
    public String getHeader(@RequestHeader("Host") String host,
                            @RequestHeader("Cookie") String cookie,
                            @CookieValue("JSESSIONID") String sessionid){
        System.out.println(host);
        System.out.println(cookie);
        System.out.println(sessionid);
        return "res";
    }

    @RequestMapping("/mytext/{name}/{id}")
    @ResponseBody
    public String text(@PathVariable String name,@PathVariable String id){
        System.out.println(name);
        System.out.println(id);
        return id;
    }

    /**json测试 */
    @RequestMapping("/getJson")
    @ResponseBody
    public List jsonTest(){
        List list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        return list;
    }

    @RequestMapping("/getMap")
    @ResponseBody
    public Map getMap(){
        HashMap<String, String> map = new HashMap<>();
        map.put("name","张三");
        map.put("学号","0056");
        return map;
    }

    @RequestMapping("/getStu")
    @ResponseBody
    public Student getStu(){
        Student student = new Student();
        student.setName("张三");
        student.setAge(18);
        Dog dog = new Dog();
        dog.setName("二狗");
        dog.setColor("黄色");
        student.setDog(dog);
        HashMap<String, String> map = new HashMap<>();
        map.put("测试","dsfd");
        map.put("测试2","csdad");
        map.put("测试3","sdd");
        student.setMymap(map);
        ArrayList list = new ArrayList();
        list.add("a");
        list.add("b");
        list.add("c");
        student.setMylist(list);
        return student;

    }

}
