package com.hys.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@ControllerAdvice
public class ExceptionController {
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public HashMap exceptionTest(){
        HashMap<String, Object> map = new HashMap<>();
        map.put("status",0);
        map.put("error","服务器错误");
        return map;
    }
}
