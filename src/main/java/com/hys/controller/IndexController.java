package com.hys.controller;

import com.hys.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class IndexController {

    @RequestMapping("/error")
    @ResponseBody
    public String error(){
        //测试异常处理      ExceptionController
        System.out.println(1/0);
        return "ok";
    }

    @RequestMapping("/test")
    public ModelAndView test(String good_name,String good_price){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("good_name",good_name);
        modelAndView.addObject("good_price",good_price);
        modelAndView.setViewName("result");
        return modelAndView;

    }

    @RequestMapping("/test1")
    public ModelAndView test1(HttpServletRequest request){
        String good_name = request.getParameter("good_name");
        String good_price = request.getParameter("good_price");

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("good_name",good_name);
        modelAndView.addObject("good_price",good_price);
        modelAndView.setViewName("result");
        return modelAndView;
    }
    @RequestMapping("/test2")
    public ModelAndView test2(@RequestParam("good_name") String name, @RequestParam("good_price") String price){

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("good_name",name);
        modelAndView.addObject("good_price",price);
        modelAndView.setViewName("result");
        return modelAndView;
    }

    @RequestMapping("/test3")
    public String test3(User user){
        return "userpage";
    }

}
