package com.hys.pojo;

import lombok.Data;

import java.util.ArrayList;
import java.util.Map;
@Data
public class Student {
    private String name;
    private Integer age;
    private ArrayList mylist;
    private Dog dog;
    private Map mymap;
}
