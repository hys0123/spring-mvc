package com.hys.pojo;

import lombok.Data;

@Data
public class Goods {
    String good_name;
    String good_price;
}
