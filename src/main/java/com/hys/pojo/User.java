package com.hys.pojo;

import lombok.Data;

import java.util.List;
@Data
public class User {
    private String user_name;
    private String user_age;
    private String hobby[];
    private Dog dog;
    private List<Dog> dogs;
}
