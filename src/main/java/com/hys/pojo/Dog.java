package com.hys.pojo;

import lombok.Data;

@Data
public class Dog {
    private String name;
    private String color;
}
